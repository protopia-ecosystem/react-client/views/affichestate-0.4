import React, {Fragment} from "react";
import BasicState from "./BasicState";
import {__} from "../utilities/i18n";
import Loading from "../utilities/Loading";
import { graphql, compose, withApollo, Query } from 'react-apollo';
import { loader } from 'graphql.macro';
import Moment from 'react-moment';
import moment from 'moment';
import $ from "jquery";
import {withRouter} from "react-router";
import { NavLink } from 'react-router-dom';
import Event from "./afficheState/Event";
import EventsFilters from "./afficheState/EventsFilters";
import { AnchorButton, Button, Classes, Dialog, Intent, Tooltip } from "@blueprintjs/core";
import {getQuery} from "../layouts/schema/graphqlf";

class AfficheState extends BasicState
{
	
	getRoute = () =>
	{
		return "affiche";
	}

	myState = route =>
	{
		//console.log(this.props);

		// return this.get_data( route.data_type, route.query);
		return this.get_data( route.data, route.query);
	}

	get_data(data_type, query)
	{
		this.query_gql = getQuery( data_type, query );

		return	<Query query={this.query_gql}>
				{
					({ loading, error, data, client}) =>
					{
						if( loading)
						{
							return <Loading/>;
						}
						if(data)
						{
							const _events = data[query] || [];
							const events = _events
								.filter(e => typeof e.title == "string" && e.title !== "")
								.map((e, i)=>
								{
									return <div key={i} className="col-lg-4 col-md-4 col-12 my-3" ><Event {...e} /></div>
								});
							return <div className="row">
								{events}
							</div>;
						}
						if(error)
						{
							return error.toString();
						}
					}
				}
				</Query>;

	}
}

export default compose(
	withApollo,
	withRouter
)(AfficheState);

